# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'gitlab-compliance-tool'
copyright = '2023, Steve Milner'
author = 'Steve Milner'
release = '0.1.1'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    'sphinx.ext.autodoc',  # somplify API documentation
    'myst_parser',         # to be able to pull in markdown
    'sphinx.ext.todo',      # show todos
]

templates_path = ['_templates']
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']
todo_include_todos = True


# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'sphinx_material'
html_theme_options = {
    'globaltoc_collapse': False,
    'color_primary': 'amber',
    'color_accent': 'deep-orange',
    'repo_url': 'https://gitlab.com/fedora/legal/gitlab-compliance-tool/',
    'base_url': 'https://fedora.gitlab.io/legal/gitlab-compliance-tool/',
    'repo_type': 'gitlab',
    'repo_name': 'gitlab-compliance-tool',
    'html_minify': True,
    'css_minify': True, 
    'nav_title': 'gitlab-compliance-tool',
}
html_sidebars = {
    "**": ["logo-text.html", "globaltoc.html", "localtoc.html", "searchbox.html"]
}
html_static_path = ['_static']
