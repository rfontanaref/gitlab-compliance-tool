gitlab-compliance-tool
======================

Simple tool to check project visibility and license listing per GitLab's 
`Open Source Program <https://about.gitlab.com/solutions/open-source/join/>`_


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   README
   license
   api


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
