API
===

gct.__init__
-------------

.. automodule:: gct.__init__
   :members:
   :special-members: __version__, __license__


gct.__main__
-------------

.. autofunction:: gct.__main__.main

.. autoclass:: gct.__main__.GitLabComplianceTool
   :members:
   :private-members:
