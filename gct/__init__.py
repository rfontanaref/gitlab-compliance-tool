# This file is part of gitlab-compliance-tool.
#
# Copyright (C) 2023 Steve Milner
#
# SPDX-License-Identifier: GPL-3.0-or-later

#: Current version of the tool
__version__ = "0.1.1"
#: The license of the project
__license__ = "GPLv3+"
